package ba.unsa.etf.sakib_ademovic.spirala3;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class FragmentPreporuci extends Fragment  {
    private Knjiga knjiga;
    private ArrayList<String> autori=new ArrayList<String>();
    Spinner spinner;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 50;
    private TextView naziv;
    private TextView autorK;
    private ImageView slika;



    public void Poslji(Knjiga k){
        knjiga = k;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_preporuci, container, false);

        spinner =(Spinner) v.findViewById(R.id.sKontakti);
        final Button posalji = (Button) v.findViewById(R.id.dPosalji);

        naziv = (TextView) v.findViewById(R.id.textView_naslov);
        autorK = ( TextView) v.findViewById(R.id.textView_autor);
        slika = (ImageView) v.findViewById(R.id.imageView_slika);

        if(knjiga.getNaziv()==null){
            naziv.setText("");
        }else {
            naziv.setText(knjiga.getNaziv());
        }

        if(knjiga.getAutori().size()==0){
            autorK.setText("");
        }else {
            autorK.setText(knjiga.getAutori().get(0).imeiPrezime );
        }

        //im.setVisibility(View.VISIBLE);
        if(knjiga.getSlika()!=null){
            Picasso.get().load(knjiga.getSlika().toString()).into(slika);
        }
        else {
            slika.setImageResource(R.drawable.ic_launcher_foreground);//Ako se ne odabere slika samo da ne izgleda prazno u prikazu
            //Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(im);
        }

        showContacts();

        posalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(spinner.getSelectedItem()!=null) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    String[] TO = {spinner.getSelectedItem().toString()};

                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    emailIntent.putExtra(Intent.EXTRA_CC, "");
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");

                    if(knjiga.getAutori().size()==0){
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Zdravo " + autori.get(spinner.getSelectedItemPosition()) + "," + "\n" + "Pročitaj knjigu " + knjiga.getNaziv()+ "!");
                    }else {
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Zdravo " + autori.get(spinner.getSelectedItemPosition()) + "," + "\n" + "Pročitaj knjigu " + knjiga.getNaziv() + " od " + knjiga.getAutori().get(0).imeiPrezime + "!");
                    }
                    try {
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return v;
    }


    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            List<String> contacts = DajEmails();
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, contacts);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(spinnerArrayAdapter);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,  int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showContacts();
            } else {
                Toast.makeText(getActivity(), "Until you grant the permission, we canot display the email", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private List<String> DajEmails() {
        ArrayList<String> nova = new ArrayList<String>();

        ContentResolver resolver = getActivity().getContentResolver();
        Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        int i = 0;
        while (cursor.moveToNext()){
            Log.d("olo ", Integer.toString(i++));
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String ime = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

            Cursor emailCursor = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);

            while (emailCursor.moveToNext()){
                String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                nova.add(email);
                autori.add(ime);
            }

        }
        return  nova;
    }
}
