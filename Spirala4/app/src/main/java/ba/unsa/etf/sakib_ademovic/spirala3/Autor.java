package ba.unsa.etf.sakib_ademovic.spirala3;

import java.util.ArrayList;

public class Autor {
    public String imeiPrezime;
    public ArrayList<String> knjige=new ArrayList<>();
    public  int napisao =0;

    public Autor(String _imeiPrezime, String _id){
        imeiPrezime=_imeiPrezime;
        knjige.add(_id);
        napisao++;
    }
    public void dodajKnjigu(String id){
        knjige.add(id);
        napisao++;
    }
    public void dodajKnjigu(){
        napisao++;
    }

    public String getImeiPrezime(){
        return imeiPrezime;
    }
    public void setImeiPrezime(String a){
        imeiPrezime=a;
    }
    public ArrayList<String> getKnjige(){
        return knjige;
    }
    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }
}
