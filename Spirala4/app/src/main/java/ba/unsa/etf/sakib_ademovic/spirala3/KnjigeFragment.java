package ba.unsa.etf.sakib_ademovic.spirala3;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 */
public class KnjigeFragment extends Fragment {

    BazaOpenHelper db;
    public KnjigeFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.activity_lista_knjiga_akt,container,false);
        Button povratk = (Button) v.findViewById(R.id.dPovratak);
        db = new BazaOpenHelper(getActivity());

        final ListView lista = (ListView) v.findViewById(R.id.listaKnjiga);
        String pozicija = KontejnerskaKlasa.izabrani;
        ArrayList<Knjiga> nova=new ArrayList<Knjiga>();

        if(KontejnerskaKlasa.dualFragment){
            povratk.setVisibility(View.GONE);
        }else{
            povratk.setVisibility(View.VISIBLE);
        }

        if(KontejnerskaKlasa.izabrani!=""){
            if(KontejnerskaKlasa.IzabraneKategorije){
                try {
                    nova=db.knjigeKategorije(db.dajIDKategorije(KontejnerskaKlasa.izabrani));
                } catch (MalformedURLException e) {
                    Toast.makeText(getActivity(),"Nije uspjelo citanje iz baze!", Toast.LENGTH_SHORT).show();
                }
            }
            else{
                try {
                    nova=db.knjigeAutora(db.dajIDAutora(KontejnerskaKlasa.izabrani));
                } catch (MalformedURLException e) {
                    Toast.makeText(getActivity(),"Nije uspjelo citanje iz baze!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        final MojAdapter mojAdapter= new MojAdapter(getActivity(), nova);
        lista.setAdapter(mojAdapter);

        final ArrayList<Knjiga> nova2 = new ArrayList<Knjiga>(nova);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Knjiga jedna = nova2.get(position);
                db.Pregledano(jedna.getId());
                nova2.get(position).Oznaci(1);
                mojAdapter.refreshEvents(nova2);
                mojAdapter.notifyDataSetChanged();
                view.setBackgroundResource(R.color.bojaZaPozadinu);
            }
        });

        povratk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        return v;
    }

}
