package ba.unsa.etf.sakib_ademovic.spirala3;

import android.graphics.Bitmap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class Knjiga  {
    private String id;
    private String naziv;
    private ArrayList<Autor> autori=new ArrayList<Autor>();
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica=0;
    private String kategorija;
    private Bitmap bitMap=null;
    private int oznacen=0;


    public Knjiga(String id,String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica){
        this.id=id;
        this.naziv = naziv;
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        this.slika=slika;
        this.brojStranica=brojStranica;
    }
    public Knjiga(String id,String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, String slika, int brojStranica){
        this.id=id;
        this.naziv = naziv;
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        try {
            this.slika= new URL(slika);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.brojStranica=brojStranica;
    }


    public boolean TrebaLiBojiti(){
        if(oznacen==1) {return true;}
        else return false;
    }
    public void Oznaci(int i){
        oznacen = i;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public Bitmap getBitMap() {
        return bitMap;
    }
    public void setBitMap(Bitmap b) {
        bitMap=b;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }
}
