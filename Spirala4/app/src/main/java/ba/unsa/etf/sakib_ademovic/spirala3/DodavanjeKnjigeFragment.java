package ba.unsa.etf.sakib_ademovic.spirala3;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;

import ba.unsa.etf.sakib_ademovic.spirala3.Autor;
import ba.unsa.etf.sakib_ademovic.spirala3.Knjiga;
import ba.unsa.etf.sakib_ademovic.spirala3.KontejnerskaKlasa;
import ba.unsa.etf.sakib_ademovic.spirala3.R;

public class DodavanjeKnjigeFragment extends Fragment {
    BazaOpenHelper db;
    private static final int PICK_IMAGE = 3;
    ImageView imv;

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }
    public DodavanjeKnjigeFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.activity_dodavanje_knjige_akt,container,false);
        db = new BazaOpenHelper(getActivity());

        final EditText ime=(EditText)v.findViewById(R.id.imeAutora);
        final EditText naslov=(EditText) v.findViewById(R.id.nazivKnjige);
        imv=(ImageView) v.findViewById(R.id.naslovnaStr);
        final Spinner spinner=(Spinner) v.findViewById(R.id.sKategorijaKnjige);
        Button DodajKnjgu=(Button) v.findViewById(R.id.dUpisiKnjigu);
        final Button DodajSliku=(Button) v.findViewById(R.id.dNadjiSliku);
        Button Ponisti=(Button) v.findViewById(R.id.dPonisti);
        final ArrayList<String> kategorije = new ArrayList<String>(db.dajKategorije());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, kategorije);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        DodajKnjgu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imv.buildDrawingCache();
                Bitmap b= Bitmap.createBitmap(imv.getDrawingCache());
                boolean error = false;
                if(b==null){
                    error = true;
                }
                if(ime.getText().toString().isEmpty()){
                    error = true;
                    ime.startAnimation(shakeError());
                }
                if(naslov.getText().toString().isEmpty()){
                    error = true;
                    naslov.startAnimation(shakeError());
                }
                if(spinner.getSelectedItem()==null){
                    error = true;
                    spinner.startAnimation(shakeError());
                }
                if(!error)
                {
                    ArrayList<Autor> autors = new ArrayList<Autor>();
                    autors.add(new Autor(ime.getText().toString(),""));
                    URL u = null;
                    Knjiga nova = new Knjiga("",naslov.getText().toString(), autors,"","",u,0);
                    nova.setKategorija(kategorije.get(spinner.getSelectedItemPosition()));
                    nova.setBitMap(b);
                    db.dodajKnjigu(nova);
                    //KontejnerskaKlasa.Biblioteka.add(nova);
                    Toast.makeText(getActivity(), "Uspijesno ste unijeli knjigu: "+naslov.getText().toString(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        DodajSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, ""), PICK_IMAGE);
            }
        });

        Ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FrameLayout l2 = (FrameLayout) getActivity().findViewById(R.id.frameLayout2);
                KontejnerskaKlasa.dodajKnjigu=false;
                getFragmentManager().popBackStack();
                if( l2 != null) {
                    l2.setVisibility(View.VISIBLE);
                }
            }
        });

        return v;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE) {
                Uri selectedImageUri = data.getData();
                if(selectedImageUri!=null)
                    imv.setImageURI(selectedImageUri);
            }
        }
    }
}
