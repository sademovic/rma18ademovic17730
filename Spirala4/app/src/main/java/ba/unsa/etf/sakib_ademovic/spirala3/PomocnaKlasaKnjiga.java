package ba.unsa.etf.sakib_ademovic.spirala3;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class PomocnaKlasaKnjiga implements Parcelable {


    public String id;
    public String naziv;
    public ArrayList<String> autori=new ArrayList<String>();
    public String opis;
    public String datumObjavljivanja;
    public String slika;
    public int brojStranica;


    public PomocnaKlasaKnjiga(String id,String naziv, ArrayList<String> autori, String opis, String datumObjavljivanja, String slika, int brojStranica){
        this.id=id;
        this.naziv = naziv;
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        this.slika=slika;
        this.brojStranica=brojStranica;
    }


    protected PomocnaKlasaKnjiga(Parcel in) {
        id = in.readString();
        naziv = in.readString();
        autori = in.createStringArrayList();
        opis = in.readString();
        datumObjavljivanja = in.readString();
        slika = in.readString();
        brojStranica = in.readInt();
    }

    public static final Creator<PomocnaKlasaKnjiga> CREATOR = new Creator<PomocnaKlasaKnjiga>() {
        @Override
        public PomocnaKlasaKnjiga createFromParcel(Parcel in) {
            return new PomocnaKlasaKnjiga(in);
        }

        @Override
        public PomocnaKlasaKnjiga[] newArray(int size) {
            return new PomocnaKlasaKnjiga[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(naziv);
        dest.writeStringList(autori);
        dest.writeString(opis);
        dest.writeString(datumObjavljivanja);
        dest.writeString(slika);
        dest.writeInt(brojStranica);
    }
}
