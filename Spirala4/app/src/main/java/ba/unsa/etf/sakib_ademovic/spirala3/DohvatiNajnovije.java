package ba.unsa.etf.sakib_ademovic.spirala3;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class DohvatiNajnovije extends AsyncTask<String,Integer,Void> {
    public interface IDohvatiNajnovijeDone{
        public void onNajnovijeDone(ArrayList<Knjiga> a);
    }
    ArrayList<Knjiga> podatci = new ArrayList<Knjiga>();
    private DohvatiNajnovije.IDohvatiNajnovijeDone pozivatelj;
    public DohvatiNajnovije(DohvatiNajnovije.IDohvatiNajnovijeDone p)
    {
        pozivatelj=p;
    }


   @Override
    protected Void doInBackground(String... strings) {
        String q=null;
        try {
            q= URLEncoder.encode(strings[0], "utf-8");
        }
        catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        String url1="https://www.googleapis.com/books/v1/volumes?q=inauthor:"+q+"&orderBy=newest&maxResults=5";
        try {
            URL url=new URL(url1);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");
            for(int i=0;i<items.length();i++){
                try{
                    JSONObject knjiga = items.getJSONObject(i);
                    JSONObject knjiga_volume = knjiga.getJSONObject("volumeInfo");
                    String id=knjiga.getString("id");
                    String naziv=knjiga_volume.getString("title");

                    ArrayList<Autor> autori_k= new ArrayList<Autor>();
                    try {
                        JSONArray autori_jo = knjiga_volume.getJSONArray("authors");
                        for(int j = 0; j<autori_jo.length();j++){
                            String sss=autori_jo.getString(j);
                            autori_k.add(new Autor(sss,id));
                        }

                    }
                    catch (Exception e){}

                    String opis ="";
                    try {
                        opis=knjiga_volume.getString("description");
                    }
                    catch(Exception e){}
                    String datum ="";
                    try {
                        datum=knjiga_volume.getString("publishedDate");
                    }
                    catch(Exception e){}
                    int br=0;
                    try {
                        br=knjiga_volume.getInt("pageCount");
                    }
                    catch(Exception e){}

                    URL slika_URL=null;
                    try {
                        JSONObject slika_jo = knjiga_volume.getJSONObject("imageLinks");
                        String slika_s = slika_jo.getString("thumbnail");
                        slika_URL = new URL(slika_s);
                    }
                    catch (Exception e){}

                    podatci.add(new Knjiga(id,naziv,autori_k,opis,datum,slika_URL,br));
                }
                catch(JSONException e){
                }
            }
        }
        catch (MalformedURLException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (JSONException e){
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(podatci);
    }

    public String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while ((line=reader.readLine())!=null){
                sb.append(line+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try{
                is.close();
            }
            catch (IOException e){

            }
        }
        return sb.toString();
    }
}
