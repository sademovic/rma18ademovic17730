package ba.unsa.etf.sakib_ademovic.spirala3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;



public class MojAdapter2 extends BaseAdapter {
    public ArrayList<Autor> str= new ArrayList<Autor>();
    public LayoutInflater mojInflater;
    BazaOpenHelper db;

    public MojAdapter2(Context c, ArrayList<Autor> s){
        str=s;
        mojInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new BazaOpenHelper(c);
    }

    @Override
    public int getCount() {
        return str.size();
    }

    @Override
    public Object getItem(int i) {
        return str.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = mojInflater.inflate(R.layout.lista_detalji2, null, false);
        TextView t1 = (TextView) v.findViewById(R.id.eNaziv);
        TextView t2 = (TextView) v.findViewById(R.id.eAutor);
        t1.setText(str.get(i).getImeiPrezime());
        t2.setText(Integer.toString(db.brojKnjiga(str.get(i).getImeiPrezime())));
        return v;
    }
}
