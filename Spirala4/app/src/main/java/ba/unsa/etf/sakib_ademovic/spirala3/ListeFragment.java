package ba.unsa.etf.sakib_ademovic.spirala3;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;


public class ListeFragment extends Fragment {
    BazaOpenHelper db;
    ArrayAdapter<String> adapter;
    MojAdapter2 adapter2;
    ListView lista;

    public void ocisti(){
        adapter.getFilter().filter("");
        adapter.notifyDataSetChanged();
    }

    public ListeFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.activity_kategorije_akt,container,false);
        db=new BazaOpenHelper(getActivity());

        final Button dKategorija = (Button) v.findViewById(R.id.dKategorije);
        final Button dAutori = (Button) v.findViewById(R.id.dAutori);
        final Button pretraga = (Button) v.findViewById(R.id.dPretraga);
        final Button dodajKategoriju = (Button) v.findViewById(R.id.dDodajKategoriju);
        final Button dodajKnjigu = (Button) v.findViewById(R.id.dDodajKnjigu);
        final Button dodajOnline = (Button) v.findViewById(R.id.dDodajOnline);

        final EditText text = (EditText) v.findViewById(R.id.tekstPretraga);
        lista = (ListView) v.findViewById(R.id.listaKategorija);
        dodajKategoriju.setEnabled(false);

        final ArrayList<String> kategorije = new ArrayList<String>(db.dajKategorije());


        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, kategorije);
        lista.setAdapter(adapter);
        KontejnerskaKlasa.IzabraneKategorije=true;


        dKategorija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, kategorije);
                lista.setAdapter(adapter);
                dodajKategoriju.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);
                text.setVisibility(View.VISIBLE);
                KontejnerskaKlasa.IzabraneKategorije=true;
            }
        });
        dAutori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter2 = new MojAdapter2(getActivity(), db.dajAutore());
                lista.setAdapter(adapter2);
                dodajKategoriju.setVisibility(View.GONE);
                pretraga.setVisibility(View.GONE);
                text.setVisibility(View.GONE);
                KontejnerskaKlasa.IzabraneKategorije=false;
            }
        });

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KontejnerskaKlasa.dodajKnjigu=true;
                DodavanjeKnjigeFragment novifragment  = new DodavanjeKnjigeFragment();
                if(KontejnerskaKlasa.dualFragment==true){
                    FrameLayout l2 = (FrameLayout) getActivity().findViewById(R.id.frameLayout2);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout1, novifragment)
                            .addToBackStack(null)
                            .commit();
                    l2.setVisibility(View.GONE);
                }
                else {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout1, novifragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(KontejnerskaKlasa.IzabraneKategorije) {
                    int s = 0;
                    adapter.getFilter().filter(text.getText().toString().trim());
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < kategorije.size(); i++) {
                        if (kategorije.get(i).toLowerCase().trim().equals(text.getText().toString().toLowerCase().trim())) {
                            s++;
                        }
                    }
                    if (s <= 0 && !text.getText().toString().isEmpty()) {
                        dodajKategoriju.setEnabled(true);
                    } else {
                        dodajKategoriju.setEnabled(false);
                    }
                }
            }
        });
        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!db.ImaLiKategorije(text.getText().toString())) {
                    db.dodajKategoriju(text.getText().toString());
                    kategorije.add(text.getText().toString());
                    adapter.add(text.getText().toString());
                    text.setText("");
                    adapter.notifyDataSetChanged();
                }
                dodajKategoriju.setEnabled(false);
                ocisti();
            }
        });
        dodajOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KontejnerskaKlasa.dodajOnline=true;
                FragmentOnline fro1 = new FragmentOnline();
                if(KontejnerskaKlasa.dualFragment == true){
                    FrameLayout l2 = (FrameLayout) getActivity().findViewById(R.id.frameLayout2);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout1, fro1)
                            .addToBackStack(null)
                            .commit();
                    l2.setVisibility(View.GONE);
                }
                else {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout1, fro1)
                            .addToBackStack(null)
                            .commit();
                }

            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(KontejnerskaKlasa.IzabraneKategorije){
                    KontejnerskaKlasa.izabrani = adapter.getItem(position);
                }
                else {
                    KontejnerskaKlasa.izabrani = ((Autor)adapter2.getItem(position)).getImeiPrezime();
                }
                KnjigeFragment novifragment  = new KnjigeFragment();
                if(KontejnerskaKlasa.dualFragment==true){
                    getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout2, novifragment)
                            .commit();
                }
                else {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout1, novifragment)
                            .addToBackStack(null)
                            .commit();
                }

            }
        });
        return v;
    }
}
