package ba.unsa.etf.sakib_ademovic.spirala3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class MojAdapter extends BaseAdapter {




    public ArrayList<Knjiga>  str= new ArrayList<Knjiga>();
    public LayoutInflater mojInflater;
    private final Context context;

    public MojAdapter(Context c, ArrayList<Knjiga> s){
        str=s;
        mojInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = c;
    }

    @Override
    public int getCount() {
        return str.size();
    }

    @Override
    public Object getItem(int i) {
        return str.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int i, final View view, ViewGroup viewGroup) {

        @SuppressLint("ViewHolder") View v = mojInflater.inflate(R.layout.lista_detalji, null, false);;
        TextView t1=(TextView) v.findViewById(R.id.eNaziv);
        TextView t2=(TextView) v.findViewById(R.id.eAutor);
        TextView t3=(TextView) v.findViewById(R.id.eOpis);
        TextView t4=(TextView) v.findViewById(R.id.eDatumObjavljivanja);
        TextView t5=(TextView) v.findViewById(R.id.eBrojStranica);
        ImageView im = (ImageView) v.findViewById(R.id.eNaslovna);
        t1.setText(str.get(i).getNaziv());
        String k = "";
        for(int j = 0; j<str.get(i).getAutori().size(); j++){
            if(j!=0) k=k+",  ";
            k=k+str.get(i).getAutori().get(j).imeiPrezime;
        }
        t2.setText( k );

        if(str.get(i).getOpis()==null){
            t3.setText("");
        }else {
            t3.setText(str.get(i).getOpis());
        }

        if(str.get(i).getDatumObjavljivanja()==null){
            t4.setText("");
        }else {
            t4.setText(str.get(i).getDatumObjavljivanja());
        }

        if(str.get(i).getBrojStranica()==0){
            t5.setText("");
        }else {
            t5.setText(Integer.toString(str.get(i).getBrojStranica()));
        }

        //im.setVisibility(View.VISIBLE);
        if(str.get(i).getSlika()!=null){
            Picasso.get().load(str.get(i).getSlika().toString()).into(im);
        }
        else if(str.get(i).getBitMap()!=null) {
            im.setImageBitmap(str.get(i).getBitMap());

        }
        else {
            im.setImageResource(R.drawable.ic_launcher_foreground);//Ako se ne odabere slika samo da ne izgleda prazno u prikazu
            //Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(im);
        }


        if(str.get(i).TrebaLiBojiti()){
            v.setBackgroundResource(R.color.bojaZaPozadinu);
        }

        final int ii = i;
        Button preporuci = (Button) v.findViewById(R.id.dPreporuci);
        preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentPreporuci novifragment  = new FragmentPreporuci();
                novifragment.Poslji(str.get(ii));
                if(KontejnerskaKlasa.dualFragment==true){
                    //FrameLayout l2 = (FrameLayout) getActivity().findViewById(R.id.frameLayout2);
                    ((FragmentActivity)context).getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout2, novifragment)
                            .addToBackStack(null)
                            .commit();
                    //l2.setVisibility(View.GONE);
                }
                else {
                    ((FragmentActivity)context).getFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout1, novifragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });


        return v;
    }

    public void refreshEvents(ArrayList<Knjiga> nova2) {
        str = nova2;
    }
}
