package ba.unsa.etf.sakib_ademovic.spirala3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.net.MalformedURLException;
import java.util.ArrayList;

public class BazaOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "SPIRALA.4";

    // Table Names
    private static final String TABLE_KATEGORIJA = "Kategorija";
    private static final String TABLE_KNJIGA = "Knjiga";
    private static final String TABLE_AUTOR = "Autor";
    private static final String TABLE_AUTORSTVO = "Autorstvo";

    // Kategorija Table - column nmaes
    private static final String KATEGORIJA_ID = "kategorija_id";
    private static final String KATEGORIJA_NAZIV = "naziv";

    // Autorstvo Table - column names
    private static final String KNJIGA_ID = "knjiga_id";
    private static final String KNJIGA_NAZIV = "naziv";
    private static final String KNJIGA_OPIS = "opis";
    private static final String KNJIGA_DATUMOBJAVLJIVANJA = "datumObjavljivanja";
    private static final String KNJIGA_BROJSTRANICA = "brojStranica";
    private static final String KNJIGA_IDWEBSERVIS = "idWebServis";
    private static final String KNJIGA_URL= "slika";
    private static final String KNJIGA_PREGLEDANO= "pregledana";
    private static final String KNJIGA_IDKATEGORIJE= "idKategorije";

    // Autor Table - column names
    private static final String AUTOR_ID = "autor_id";
    private static final String AUTOR_IME = "ime";

    // Autorstvo Table - column names
    private static final String AUTORSTVO_ID = "autorstvo_id";
    private static final String AUTORSTVO_AUTOR = "idautora";
    private static final String AUTORSTVO_KNJIGA = "idknjige";

    // Table Create Statements
    // Kategorije table create statement
    private static final String CREATE_TABLE_KATEGORIJE = "CREATE TABLE "
            + TABLE_KATEGORIJA +
            "(" + KATEGORIJA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KATEGORIJA_NAZIV + " TEXT" + ")";

    // Autor table create statement
    private static final String CREATE_TABLE_AUTORI = "CREATE TABLE " + TABLE_AUTOR
            + "(" + AUTOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + AUTOR_IME + " TEXT" + ")";

    // AUTORSTVO table create statement
    private static final String CREATE_TABLE_AUTORSTVO = "CREATE TABLE "
            + TABLE_AUTORSTVO +
            "(" + AUTORSTVO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + AUTORSTVO_AUTOR + " INTEGER,"
            + AUTORSTVO_KNJIGA + " INTEGER" + ")";

    // Knjiga table create statement
    private static final String CREATE_TABLE_KNJIGA = "CREATE TABLE "
            + TABLE_KNJIGA +
            "(" + KNJIGA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KNJIGA_NAZIV + " TEXT,"
            + KNJIGA_OPIS + " TEXT,"
            + KNJIGA_DATUMOBJAVLJIVANJA + " TEXT,"
            + KNJIGA_BROJSTRANICA + " INTEGER,"
            + KNJIGA_IDWEBSERVIS + " TEXT,"
            + KNJIGA_URL + " TEXT,"
            + KNJIGA_PREGLEDANO + " INTEGER,"
            + KNJIGA_IDKATEGORIJE + " INTEGER" + ")";


    public BazaOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_KATEGORIJE);
        db.execSQL(CREATE_TABLE_KNJIGA);
        db.execSQL(CREATE_TABLE_AUTORI);
        db.execSQL(CREATE_TABLE_AUTORSTVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_KATEGORIJA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTORSTVO);
        onCreate(db);
    }

    public long  dodajKategoriju(String naziv){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KATEGORIJA_NAZIV,naziv);
        long id = db.insert(TABLE_KATEGORIJA,null,contentValues);
        return id;
    }

    public ArrayList<String> dajKategorije(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c= db.rawQuery("SELECT * FROM " + TABLE_KATEGORIJA,null);
        ArrayList<String> res = new ArrayList<String>();

        while (c.moveToNext()){
            int i = c.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
            res.add(c.getString(i));
        }
        c.close();

        return  res;
    }

    public long dajIDKategorije(String naziv){
        String selectQuery1 = "SELECT " +KATEGORIJA_ID+
                " FROM " + TABLE_KATEGORIJA
                +" WHERE " + KATEGORIJA_NAZIV + " LIKE '" + naziv+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery1,null);
        if(c.getCount() > 0) {
            c.moveToFirst();
            int res = c.getInt(c.getColumnIndex(KATEGORIJA_ID));
            c.close();
            return res;
        }
        c.close();
        return -1;
    }


    public boolean ImaLiKategorije(String naziv){
        String selectQuery1 = "SELECT " +KATEGORIJA_ID+
                " FROM " + TABLE_KATEGORIJA
                +" WHERE " + KATEGORIJA_NAZIV + " LIKE '" + naziv+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery1,null);
        if(c.getCount() > 0) {
            return  true;
        }
        c.close();
        return false;
    }

    public boolean ImaLiKnjiga(String naziv){
        String selectQuery1 = "SELECT " +KNJIGA_ID+
                " FROM " + TABLE_KNJIGA
                +" WHERE " + KNJIGA_NAZIV+ " = " + android.database.DatabaseUtils.sqlEscapeString(naziv);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery1,null);
        if(c.getCount() > 0) {
            return  true;
        }
        c.close();
        return false;
    }

    public  String dajNazivKategorije(long id){
        String selectQuery1 = "SELECT " +KATEGORIJA_NAZIV+
                " FROM " + TABLE_KATEGORIJA
                +" WHERE " + KATEGORIJA_ID + " = " + Integer.toString((int) id);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery1,null);
        if(c.getCount() > 0) {
            c.moveToFirst();
            String res = c.getString(c.getColumnIndex(KATEGORIJA_NAZIV));
            c.close();
            return res;
        }
        c.close();
        return "";
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKategorije) throws MalformedURLException {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * " +
                " FROM " + TABLE_KNJIGA
                +" WHERE " + KNJIGA_IDKATEGORIJE + " = " + Integer.toString((int) idKategorije) ;

        Cursor c= db.rawQuery( selectQuery ,null);
        ArrayList<Knjiga> res = new ArrayList<Knjiga>();

        while (c.moveToNext()){
            int idK = c.getInt(c.getColumnIndex(KNJIGA_ID));
            String naziv = c.getString(c.getColumnIndex(KNJIGA_NAZIV));
            String opis = c.getString(c.getColumnIndex(KNJIGA_OPIS));
            String datum = c.getString(c.getColumnIndex(KNJIGA_DATUMOBJAVLJIVANJA));
            int brStranica = c.getInt(c.getColumnIndex(KNJIGA_BROJSTRANICA));
            String web = c.getString(c.getColumnIndex(KNJIGA_IDWEBSERVIS));
            String slika = c.getString(c.getColumnIndex(KNJIGA_URL));
            int IdKategorie = c.getInt(c.getColumnIndex(KNJIGA_IDKATEGORIJE));
            String kategorija = dajNazivKategorije(IdKategorie);
            int oznaceno = c.getInt(c.getColumnIndex(KNJIGA_PREGLEDANO));

            ArrayList<Autor> autors = new ArrayList<Autor>();
            String selectQuery2 = "SELECT DISTINCT " + AUTOR_IME +", "+AUTORSTVO_KNJIGA
                    +" FROM " + TABLE_AUTOR +" a ,"+TABLE_AUTORSTVO +" au"
                    +" WHERE " + idK + " = " + "au."+AUTORSTVO_KNJIGA+ " AND "
                                + "au."+AUTORSTVO_AUTOR+ " = a."+AUTOR_ID ;
            Cursor c2= db.rawQuery( selectQuery2 ,null);

            while (c2.moveToNext()){
                String ime = c2.getString(c2.getColumnIndex(AUTOR_IME));
                autors.add(new Autor(ime,""));

            }
            res.add(new Knjiga(web, naziv, autors, opis,datum,slika, brStranica ));
            if(res.size()!=0) res.get(res.size()-1).setKategorija(kategorija);
            if(oznaceno==1) res.get(res.size()-1).Oznaci(1);
        }
        c.close();
        return  res;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora) throws MalformedURLException {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT DISTINCT "+KNJIGA_ID + ", "+ KNJIGA_NAZIV+ ", "+KNJIGA_OPIS+
                ", "+KNJIGA_DATUMOBJAVLJIVANJA+ ", "+KNJIGA_BROJSTRANICA+ ", "+KNJIGA_IDWEBSERVIS+
                ", "+KNJIGA_IDKATEGORIJE+", "+KNJIGA_URL+", "+KNJIGA_PREGLEDANO+
                " FROM " + TABLE_KNJIGA+" , " + TABLE_AUTORSTVO +", " + TABLE_AUTOR+
                " WHERE " + Integer.toString((int)idAutora)+ " = " +AUTORSTVO_AUTOR +
                        " AND " +  AUTORSTVO_KNJIGA + " = " + KNJIGA_ID;

        Cursor c= db.rawQuery( selectQuery ,null);
        ArrayList<Knjiga> res = new ArrayList<Knjiga>();

        while (c.moveToNext()){
            int idK = c.getInt(c.getColumnIndex(KNJIGA_ID));
            String naziv = c.getString(c.getColumnIndex(KNJIGA_NAZIV));
            String opis = c.getString(c.getColumnIndex(KNJIGA_OPIS));
            String datum = c.getString(c.getColumnIndex(KNJIGA_DATUMOBJAVLJIVANJA));
            int brStranica = c.getInt(c.getColumnIndex(KNJIGA_BROJSTRANICA));
            String web = c.getString(c.getColumnIndex(KNJIGA_IDWEBSERVIS));
            String slika = c.getString(c.getColumnIndex(KNJIGA_URL));
            int IdKategorie = c.getInt(c.getColumnIndex(KNJIGA_IDKATEGORIJE));
            String kategorija = dajNazivKategorije(IdKategorie);
            int oznaceno = c.getInt(c.getColumnIndex(KNJIGA_PREGLEDANO));

            ArrayList<Autor> autors = new ArrayList<Autor>();
            String selectQuery2 = "SELECT DISTINCT " + AUTOR_IME +", "+AUTORSTVO_KNJIGA
                    +" FROM " + TABLE_AUTOR +" a ,"+TABLE_AUTORSTVO +" au"
                    +" WHERE " + idK + " = " + "au."+AUTORSTVO_KNJIGA+ " AND "
                    + "au."+AUTORSTVO_AUTOR+ " = a."+AUTOR_ID ;
            Cursor c2= db.rawQuery( selectQuery2 ,null);

            while (c2.moveToNext()){
                String ime = c2.getString(c2.getColumnIndex(AUTOR_IME));
                autors.add(new Autor(ime,""));

            }
            res.add(new Knjiga(web, naziv, autors, opis,datum,slika, brStranica ));
            if(res.size()!=0) res.get(res.size()-1).setKategorija(kategorija);
            if(oznaceno==1) res.get(res.size()-1).Oznaci(1);
        }

        c.close();
        return  res;
    }

    public ArrayList<Autor> dajAutore(){
        ArrayList<Autor> res = new ArrayList<Autor>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c= db.rawQuery("SELECT * FROM " + TABLE_AUTOR,null);

        while (c.moveToNext()){
            int i = c.getColumnIndexOrThrow(AUTOR_IME);
            res.add(new Autor(c.getString(i),""));
        }
        c.close();
        return  res;
    }

    private boolean imaLiAutora(String ime){
        ArrayList<Autor> autors = dajAutore();

        for (int i = 0; i < autors.size(); i++) {
            if(autors.get(i).imeiPrezime.equals(ime)){
                return true;
            }
        }

        return false;
    }

    public void Pregledano(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues c = new ContentValues();
        c.put(KNJIGA_PREGLEDANO,1);
        db.update(TABLE_KNJIGA,c,KNJIGA_IDWEBSERVIS+" = "+android.database.DatabaseUtils.sqlEscapeString(id),null);
    }

    public int dajIDAutora(String naziv){
        String selectQuery1 = "SELECT " +AUTOR_ID+
                " FROM " + TABLE_AUTOR
                +" WHERE " + AUTOR_IME + " LIKE " + android.database.DatabaseUtils.sqlEscapeString(naziv);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery1,null);

        if(c.getCount() > 0) {
            c.moveToFirst();
            int res = c.getInt(c.getColumnIndex(AUTOR_ID));
            c.close();
            return res;
        }
        c.close();
        return -1;
    }

    public int brojKnjiga(String naziv){
        SQLiteDatabase db = this.getReadableDatabase();
        int id = dajIDAutora(naziv);
        Cursor c= db.rawQuery("SELECT DISTINCT " +AUTORSTVO_ID +  " FROM " + TABLE_AUTORSTVO+
                " WHERE "+AUTORSTVO_AUTOR+" = "+id,null);

        int count=0;
        c.moveToNext();
        count=c.getCount();
        return count;
    }

    public long dodajKnjigu(Knjiga knjiga) {
        long res = -1;
        if(!ImaLiKnjiga(knjiga.getNaziv())){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put(KNJIGA_NAZIV, knjiga.getNaziv());
            contentValues2.put(KNJIGA_OPIS, knjiga.getOpis());
            contentValues2.put(KNJIGA_DATUMOBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
            contentValues2.put(KNJIGA_BROJSTRANICA, knjiga.getBrojStranica());
            contentValues2.put(KNJIGA_IDWEBSERVIS, knjiga.getId());

            long idK = this.dajIDKategorije(knjiga.getKategorija());
            if(knjiga.getSlika()!=null) contentValues2.put(KNJIGA_URL, knjiga.getSlika().toString());
            else contentValues2.put(KNJIGA_URL, "");
            if(knjiga.TrebaLiBojiti()) contentValues2.put(KNJIGA_PREGLEDANO, 1);
            else contentValues2.put(KNJIGA_PREGLEDANO, 0);
            contentValues2.put(KNJIGA_IDKATEGORIJE, (int) idK);
            res = db.insert(TABLE_KNJIGA, null, contentValues2);

            ArrayList<Autor> trenutni = new ArrayList<Autor>();
            trenutni = dajAutore();

            for (int i = 0; i < knjiga.getAutori().size(); i++) {
                long res2= -1;
                if (!imaLiAutora(knjiga.getAutori().get(i).imeiPrezime)) {
                    ContentValues contentValues3 = new ContentValues();
                    contentValues3.put(AUTOR_IME,knjiga.getAutori().get(i).imeiPrezime);
                    res2 = db.insert(TABLE_AUTOR,null,contentValues3);
                }
                else {
                    res2 = dajIDAutora(knjiga.getAutori().get(i).imeiPrezime);
                }
                if(res2!=-1){
                    ContentValues contentValues4 = new ContentValues();
                    contentValues4.put(AUTORSTVO_AUTOR,res2);
                    contentValues4.put(AUTORSTVO_KNJIGA,res);
                    long res3 = db.insert(TABLE_AUTORSTVO,null,contentValues4);
                }

            }

        }
        return res;
    }
}