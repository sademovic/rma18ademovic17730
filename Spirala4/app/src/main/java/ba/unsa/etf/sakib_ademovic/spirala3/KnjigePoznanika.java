package ba.unsa.etf.sakib_ademovic.spirala3;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class KnjigePoznanika extends IntentService {
    public static final int STATUS_START =0,  STATUS_FINISH =1,  STATUS_ERROR=2;
    Boolean treba_slati = false;

    public KnjigePoznanika() {
        super(null);
    }

    public KnjigePoznanika(String name) {
        super(name);

    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        receiver.send(STATUS_START,bundle.EMPTY);

        ArrayList<PomocnaKlasaKnjiga> podatci = new ArrayList<PomocnaKlasaKnjiga>();
        String id=intent.getStringExtra("idKorisnika");
        try {
            String url1="https://www.googleapis.com/books/v1/users/" + id + "/bookshelves";
            String id2=null;
            URL url_2=new URL(url1);
            HttpsURLConnection urlConnection_2 = (HttpsURLConnection) url_2.openConnection();
            urlConnection_2.setRequestMethod("GET");
            InputStream in_2 = new BufferedInputStream(urlConnection_2.getInputStream());
            String rezultat_2 = convertStreamToString(in_2);
            JSONObject jo = new JSONObject(rezultat_2);
            JSONArray items_2 = jo.getJSONArray("items");
            for(int i=0;i<items_2.length();i++) {
                try {
                    JSONObject polica = items_2.getJSONObject(i);
                    id2 = polica.getString("id");
                    podatci.addAll(fija(id,id2));
                }
                catch (Exception e){
                    treba_slati=true;
                }
            }

        }
        catch (Exception e){
            treba_slati = true;
        }
        if(treba_slati){
            receiver.send(STATUS_ERROR, bundle.EMPTY);
        }
        else {
            bundle.putParcelableArrayList("listaKnjiga", podatci);
            receiver.send(STATUS_FINISH, bundle);
        }

    }

    public ArrayList<PomocnaKlasaKnjiga> fija(String idK, String id2){
        ArrayList<PomocnaKlasaKnjiga> podatci = new ArrayList<PomocnaKlasaKnjiga>();


        try {
            String url2 ="https://www.googleapis.com/books/v1/users/"+idK+"/bookshelves/"+id2+"/volumes";
            URL url=new URL(url2);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");
            for(int i=0;i<items.length();i++){
                try{
                    JSONObject knjiga = items.getJSONObject(i);
                    JSONObject knjiga_volume = knjiga.getJSONObject("volumeInfo");
                    String id=knjiga.getString("id");
                    String naziv=knjiga_volume.getString("title");

                    ArrayList<String> autori_k= new ArrayList<String>();

                    try {
                        JSONArray autori_jo = knjiga_volume.getJSONArray("authors");
                        for (int j = 0; j < autori_jo.length(); j++) {
                            String sss = autori_jo.getString(j);
                            autori_k.add(sss);
                        }
                    }
                    catch (Exception e){}

                    String opis = "";
                    try {
                        opis=knjiga_volume.getString("description");
                    }
                    catch(Exception e){}
                    String datum ="";
                    try {
                        datum=knjiga_volume.getString("publishedDate");
                    }
                    catch(Exception e){}
                    int br=0;
                    try {
                        br=knjiga_volume.getInt("pageCount");
                    }
                    catch(Exception e){}

                    String slika_URL=null;
                    try {
                        JSONObject slika_jo = knjiga_volume.getJSONObject("imageLinks");
                        Log.d("praitim", "zena");
                        slika_URL = slika_jo.getString("thumbnail");
                    }
                    catch (Exception e){
                    }

                    podatci.add(new PomocnaKlasaKnjiga(id,naziv,autori_k,opis,datum,slika_URL,br));
                }
                catch(JSONException e){

                }
            }
        }
        catch (Exception e){
            treba_slati=true;
        }
        return  podatci;
    }

    public String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while ((line=reader.readLine())!=null){
                sb.append(line+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try{
                is.close();
            }
            catch (IOException e){

            }
        }
        return sb.toString();
    }
}
