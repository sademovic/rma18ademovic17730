package ba.unsa.etf.sakib_ademovic.spirala3;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;


public class KategorijeAkt extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        KontejnerskaKlasa.dualFragment = false;
        FragmentManager manager = getFragmentManager();
        FrameLayout dioZaKnjige = (FrameLayout) findViewById(R.id.frameLayout2);
        if (findViewById(R.id.frameLayout2) != null) {
            KontejnerskaKlasa.dualFragment = true;
        }


        if(!KontejnerskaKlasa.dodajKnjigu && !KontejnerskaKlasa.dodajOnline){
            if (findViewById(R.id.frameLayout2) != null) {
                KontejnerskaKlasa.dualFragment = true;
                KnjigeFragment drugiFragment;
                drugiFragment = (KnjigeFragment) manager.findFragmentById(R.id.frameLayout2);
                FragmentTransaction tras = manager.beginTransaction();

                if (drugiFragment == null) {
                    drugiFragment = new KnjigeFragment();
                    tras.replace(R.id.frameLayout2, drugiFragment);
                    tras.commit();
                }
            }
            ListeFragment prvifragment = null;
            try {
                prvifragment = (ListeFragment) manager.findFragmentById(R.id.frameLayout1);
            } catch (Exception e) {
            }

            if (prvifragment == null) {
                FragmentTransaction tras = manager.beginTransaction();
                prvifragment = new ListeFragment();
                tras.replace(R.id.frameLayout1, prvifragment);
                tras.commit();
            } else {
                manager.popBackStack();
            }
        }
        else{
            if(KontejnerskaKlasa.dodajKnjigu){
                if(KontejnerskaKlasa.dualFragment){
                    findViewById(R.id.frameLayout2).setVisibility(View.GONE);
                }
                DodavanjeKnjigeFragment dkf1 = null;
                try {
                    dkf1 = (DodavanjeKnjigeFragment) manager.findFragmentById(R.id.frameLayout1);
                } catch (Exception e) {}
                if(dkf1==null){
                    DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.frameLayout1, dkf);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
            else if(KontejnerskaKlasa.dodajOnline){
                if(KontejnerskaKlasa.dualFragment){
                    findViewById(R.id.frameLayout2).setVisibility(View.GONE);
                }
                FragmentOnline dkf1 = null;
                try {
                    dkf1 = (FragmentOnline) manager.findFragmentById(R.id.frameLayout1);
                } catch (Exception e) {}
                if(dkf1==null){
                    FragmentOnline dkf = new FragmentOnline();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.frameLayout1, dkf);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onBackPressed() {
        if (KontejnerskaKlasa.dualFragment) {
            FrameLayout l2 = (FrameLayout) findViewById(R.id.frameLayout2);
            if( l2 != null) {
                l2.setVisibility(View.VISIBLE);
            }
        }
        KontejnerskaKlasa.dodajKnjigu=false;
        KontejnerskaKlasa.dodajOnline=false;
        super.onBackPressed();
    }
}
