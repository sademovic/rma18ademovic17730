package ba.unsa.etf.sakib_ademovic.spirala3;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone,MojResultReceiver.Receiver {
    ArrayList<Knjiga> podatci = new ArrayList<Knjiga>();
    Spinner spinnerRez;
    BazaOpenHelper db;

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    public String[] Porocesuiraj(String s,String spliter){
        String dijelovi[] = s.split(spliter);
        return dijelovi;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_fragment_online, container, false);
        db=new BazaOpenHelper(getActivity());

        final EditText text = (EditText) v.findViewById(R.id.textUpit);
        final Spinner spinnerKat = (Spinner) v.findViewById(R.id.sKategorije​);
        spinnerRez = (Spinner) v.findViewById(R.id.sRezultat);
        Button pretraga = (Button) v.findViewById(R.id.dRun);
        Button dodajKnjigu = (Button) v.findViewById(R.id.dAdd);
        final Button povratak = (Button) v.findViewById(R.id.dPovratak);

        final ArrayList<String> kategorije = new ArrayList<String>(db.dajKategorije());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, kategorije);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKat.setAdapter(spinnerArrayAdapter);

        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                podatci.clear();
                boolean error = false;
                if(text.getText().toString().isEmpty()){
                    error = true;
                    text.startAnimation(shakeError());
                }
                if(!error) {

                    String kako[]= Porocesuiraj(text.getText().toString(),":");

                    if(kako.length==1){

                        String trazi[]= Porocesuiraj(text.getText().toString(),";");
                        for(int i = 0; i<trazi.length;i++){
                            new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(trazi[i]);
                        }
                    }

                    else if(kako[0].toString().equals("autor")){
                        new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone) FragmentOnline.this).execute(kako[1]);
                    }
                    else if(kako[0].toString().equals("korisnik")){
                        Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(),KnjigePoznanika.class);
                        MojResultReceiver mReceiver = new MojResultReceiver(new Handler());
                        mReceiver.setReceiver((MojResultReceiver.Receiver) FragmentOnline.this);
                        intent.putExtra("idKorisnika", kako[1]);
                        intent.putExtra("receiver",mReceiver);
                        getActivity().startService(intent);
                    }
                }
            }
        });


        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error=false;
                if(spinnerRez.getSelectedItem()==null){
                    error = true;
                    spinnerRez.startAnimation(shakeError());
                }
                if(spinnerKat.getSelectedItem()==null){
                    error = true;
                    spinnerKat.startAnimation(shakeError());
                }
                if(!error){
                    for(int i=0; i<podatci.size();i++){
                        if(podatci.get(i).getNaziv().equals(spinnerRez.getSelectedItem())){

                            podatci.get(i).setKategorija(spinnerKat.getSelectedItem().toString());
                            db.dodajKnjigu(podatci.get(i));
                            //KontejnerskaKlasa.Biblioteka.add(podatci.get(i));
                            Toast.makeText(getActivity(), "Uspijesno ste unijeli knjigu!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KontejnerskaKlasa.dodajOnline=false;
                FrameLayout l2 = (FrameLayout) getActivity().findViewById(R.id.frameLayout2);
                getFragmentManager().popBackStack();
                if( l2 != null) {
                    l2.setVisibility(View.VISIBLE);
                }
            }
        });

        return v;
    }

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> a) {
        podatci.addAll(a);
        ArrayList<String> nova = new ArrayList<String>();
        for(int i = 0; i<podatci.size(); i++){
            nova.add(podatci.get(i).getNaziv());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, nova);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRez.setAdapter(spinnerArrayAdapter);
        Toast.makeText(getActivity(), "Ucitano je: "+Integer.toString(nova.size()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> a) {
        podatci.addAll(a);
        ArrayList<String> nova = new ArrayList<String>();
        for(int i = 0; i<podatci.size(); i++){
            nova.add(podatci.get(i).getNaziv());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, nova);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRez.setAdapter(spinnerArrayAdapter);
        Toast.makeText(getActivity(), "Ucitano je: "+Integer.toString(nova.size()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case KnjigePoznanika.STATUS_START:
                break;
            case KnjigePoznanika.STATUS_FINISH:
                podatci.clear();
                ArrayList<PomocnaKlasaKnjiga> items = new ArrayList<PomocnaKlasaKnjiga>();
                items = resultData.getParcelableArrayList("listaKnjiga");
                ArrayList<Autor> autorii = new ArrayList<Autor>();
                if(items!=null) {
                    for (int i = 0; i < items.size(); i++) {
                        autorii.clear();
                        for (int j = 0; j < items.get(i).autori.size(); j++) {
                            autorii.add(new Autor(items.get(i).autori.get(j), items.get(i).id));
                        }
                        URL url = null;
                        try {
                            url = new URL(items.get(i).slika);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        podatci.add(new Knjiga(items.get(i).id, items.get(i).naziv, autorii, items.get(i).opis, items.get(i).datumObjavljivanja, url, items.get(i).brojStranica));
                        Log.d("prat",podatci.get(i).getNaziv() + " -- " +Integer.toString(podatci.get(i).getAutori().size()));
                    }
                }

                ArrayList<String> nova = new ArrayList<String>();
                for(int i = 0; i<podatci.size(); i++){
                    nova.add(podatci.get(i).getNaziv());
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, nova);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerRez.setAdapter(spinnerArrayAdapter);
                Toast.makeText(getActivity(), "Ucitano je: "+Integer.toString(nova.size()), Toast.LENGTH_SHORT).show();

                break;
            case KnjigePoznanika.STATUS_ERROR:
                Toast.makeText(getActivity(), "STATUS_ERROR", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
